import platform
import glob
import os


def get_python_version():

	'''Gets python version 
		
	Args: None
	Returns (str): Python Version at execution

	'''
	version = platform.python_version()
	lst = version.split('.')
	return lst[0] 



def detect_reqtxt():

	'''detect requirements.txt within folder
	MUST be run from project root.
	
	Args: None
	Returns (int): 0 if requirements.txt absent, 1 otherwise

	'''
	res = glob.glob('requirements.txt')
	if (len(res)==1):
		return 1
	return 0


def generate_reqtxt():

	'''generates swrequirements.txt - by scanning packages in use and
	comparing it with pypi available packages. 
	MUST be run from project root.

	Args: None
	Returns(int): 1 for completion, 0 for error with pipreqs

	'''

	if os.path.isdir(".__spacewire__"):
		os.system("rm -rf .__spacewire__")
	os.system('mkdir .__spacewire__')
	cwd = os.getcwd()
	cmd = 'pipreqs --savepath {}/.__spacewire__/swpiprequirements.txt {}'.format(cwd,cwd)
	try:	
		os.system(cmd)
	except:
		return 0
	return 1

def get_conda_dependencies():

	'''generates all non-pip dependencies that the code has access to
	MUST be run from project root
	Args: None
	Returns(int): 1 for completion, 0 for failure
	'''

	cwd = os.getcwd()
	cmd = 'conda list --export --no-pip > {}/.__spacewire__/swcondadependecies.txt'.format(cwd)
	try:
		os.system(cmd)
	except:
		return 0
	return 1



def sel_env():

	python_version = get_python_version()
	hasreqtxt = detect_reqtxt()
	if hasreqtxt==1:
		reqtxt = '{}/requirements.txt'.format(os.getcwd())
	else:
		generate_reqtxt()
		reqtxt = '{}/.__spacewire__/swpiprequirements.txt'.format(os.getcwd())
	reqlist = []
	file = open(reqtxt, 'r')
	for line in file:
		reqlist.append(line)
	file.close()
	reqlist = map(str.rstrip,reqlist)
	reqdict = [{}]
	for item in reqlist:
		try:
			key,val = item.split('==',1)
		except:
			if python_version==2:
				return 'py27',None
			else:
				return 'py36',None

		if key in reqdict[-1]:
			reqdict.append({})
		reqdict[-1][key] = val
	result = reqdict[0]



	if 'tensorflow' in result.keys():
		ver = result['tensorflow'].split('.')
		if ver[0]==0:
			if python_version=='2':
				return 'keras1tf0py2',reqtxt 
			else:
				return 'keras1tf0py3',reqtxt
		else:
			if python_version=='2':
				return 'keras2tf1py2',reqtxt
			else:
				return 'keras2tf1py3',reqtxt

	if 'torch' in result.keys():
		if python_version=='2':
			return 'pytorchpy2',reqtxt
		return 'pytorchpy3',reqtxt


	if 'Theano' in result.keys():
		ver = result['Theano'].split('.')
		if ver[0]==0:
			if python_version=='2':
				return 'theano9keras1py2',reqtxt
			else:
				return 'theano9keras1py3',reqtxt

		else:
			if python_version=='2':
				return 'theano1keras2py2',reqtxt
			else:
				return 'theano1keras2py3',reqtxt

	if python_version=='2':
		return ' py27',reqtxt
	return 'py36',reqtxt

def main():

	commands = []
	env,reqtxt = sel_env()

	envactivate = 'source activate {}'.format(env)
	commands.append(envactivate)
	if reqtxt:
		pipinstall = 'pip install -r {}'.format(reqtxt)
		commands.append(pipinstall)
	return commands














