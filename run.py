import sys, os
import click
import requests
import time
import json
import itertools
import subprocess
import detect
from localstorage import Config
import tempfile
import paramiko
import time

url = 'http://backend.spacewire.io/api'

pass_config = click.make_pass_decorator(Config, ensure=True)

#MAIN

@click.group()
def main():
	'''
	This is the terminal command line tool for SpaceWire.
	Compute in the cloud without compromising convenience.
	'''
	pass

#PYTHON

@main.command()
@pass_config
@click.pass_context
@click.argument('script', type=click.Path(exists=True))
def python(ctx,config, script):
	'''
	Runs a python script in the cloud.
	'''
	make_rocket()

	click.echo('Generating Environment...')

	environment = detect.main()

	files = {'file': environment}
	r = requests.post(url+'/submit_project', files=files,
		auth=(config.username, config.password), timeout=20)
	if r.status_code == 500:
		click.echo('No workers available!! System overloaded :(')
		return

	worker_id = r.json()['workerid']
	click.echo('Worker ID: ' + worker_id)

	print('Setting up container...', end='')
	spinner = itertools.cycle(['\\', '|', '/', '-'])
	while not container_ready(worker_id):

		sys.stdout.write(spinner.__next__())
		sys.stdout.flush()
		time.sleep(0.5)
		sys.stdout.write('\b')

	click.echo('')

	container_info = get_container_info(worker_id)
	click.echo(container_info)

	#Now that the container info is present, it'd be
	#good to have a pem file ready. go ahead and
	#write that one.

	#this is the temp file we will store the rsa
	#key into.
	temp_key_file = tempfile.NamedTemporaryFile()
	temp_key_file.write(container_info['key'].encode('utf-8'))
	click.echo('temp file: ' + temp_key_file.name)

	#now that the container is ready, go ahead and
	#rsync it in.
	temp_key_file.seek(0)
	print(temp_key_file.read().decode('utf-8'))
	temp_key_file.seek(0)

	#'/Users/samcrane/Documents/GitHub/swcli/final_key.pem'
	key = temp_key_file.name
	host = container_info['ip_address']
	user = 'root'#container_info['username']
	port = container_info['port']

	#once Rsync is done, ssh in and run the command.

	# command = 'su -S rootpass'
	#run_container_command(key, user, host, port, command)
	#run_container_command(key, user, host, port, 'virtualenv venv')
	#run_container_command(key, user, host, port, 'source venv/bin/activate')
	
	rsync_to_container(key, user, host, port)

	command = 'cd /home/; pip install -r .__spacewire__/swpiprequirements.txt'
	run_container_command(key, user, host, port, command)

	command = 'cd /home/; python3 {0}'.format(click.format_filename(script))
	run_container_command(key, user, host, port, command)

	command = 'cd /home/; ls'
	run_container_command(key, user, host, port, command)

	rsync_from_container(key, user, host, port)	

	temp_key_file.close()

def get_container_info(worker_id):
	try:
		r = requests.post(url+'/get_container_status', 
			json={'worker_id':worker_id}, timeout=20)
		if r.status_code == 500:
			click.echo('Error creating container.')
			print(r.text)
			return False
	except requests.exceptions.Timeout:
		click.echo('Request timedout.')
		return False

	return r.json()


def container_ready(worker_id):
	''' Longpolls until the container is ready
		for loading files.
	'''
	try:
		r = requests.post(url+'/get_container_status', 
			json={'worker_id':worker_id}, timeout=20)
		if r.status_code == 500:
			click.echo('Error creating container.')
			print(r.text)
			return False
	except requests.exceptions.Timeout:
		click.echo('Request timedout.')
		return False
	json_response = r.json()

	status = json_response['status']

	return status != False

def run_container_command(key, user, host, port, command):
	''' If you can ssh into the container,
	go ahead and read the screen.'''
	# proc = subprocess.Popen(["ssh", "-i", key, "{0}@{1}".format(user,host), "-p", port, "-t", command],
	# 				   stdout=subprocess.PIPE,
	# 				   stderr=subprocess.PIPE)
	key = '/Users/samcrane/Documents/GitHub/keys/test.pem'
	custom_ssh = "ssh -i {0} {1}@{2} -p {3} ".format(key, user, host, port) #gives port, key
	command = custom_ssh + command
	print(command)

	proc = subprocess.check_call(command, shell=True)

	# s = paramiko.SSHClient()
	# pkey = paramiko.RSAKey.from_private_key_file(key)
	# s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	# s.connect(host, port=port, username=user, pkey=pkey)
	# (stdin, stdout, stderr) = s.exec_command(command, get_pty=True)
	# # for line in stdout.readlines():
	# # 	print(line)
	# # s.close()

	# try:
	# 	# for l in line_buffered(stdout):
	# 	# 	print(l,end='')
	# 	for line in iter(stdout.readline,""): 
	# 		print(line,end="")
	# except KeyboardInterrupt:
	# 	click.echo('Connection Closing...')
	# 	#in this place we should go ahead and shut down the container.
	# 	pass

	# return None

def line_buffered(f):
	line_buf = ""
	while not f.channel.exit_status_ready():
		line_buf += f.read(1).decode('utf-8')
		if line_buf.endswith('\n'):
			yield line_buf
			line_buf = ''

def rsync_to_container(key, user, host, port):
	''' Check to see if the files have been
	completely created. 
	'''
	cwd = os.getcwd()+'/'
	custom_ssh = "\'ssh -i {0} -p {1}\'".format(key, port) #gives port, key
	remote_host= "{0}@{1}:/home/".format(user, host) #user, host
	command = ['rsync', '-avz', cwd, '-e', custom_ssh, '--progress', remote_host]
	#command = ['rsync', '-p', str(port), "-e", "\'ssh -i {0}\'".format(key), '$(pwd)', '{0}@{1}:/'.format(user,host)]
	print(' '.join(command))
	#os.system(' '.join(command))

	# proc = subprocess.Popen(command,
	# 					   shell=False,
 #                           stdout=subprocess.PIPE,
 #                           stderr=subprocess.PIPE
 #                           )
	proc = subprocess.check_call(' '.join(command), shell=True)
	# while True:
	# 	line = proc.stdout.readline()
	# 	print('check')
	# 	if line != b'':
	# 		#the real code does filtering here
	# 		print(line.rstrip().decode('utf-8'))
	# 	else:
	# 		break


def rsync_from_container(key, user, host, port):
	''' Check to see if the files have been
	completely created. 
	'''
	cwd = os.getcwd()+'/'
	custom_ssh = "\'ssh -i {0} -p {1}\'".format(key, port) #gives port, key
	remote_host= "{0}@{1}:/home/".format(user, host) #user, host
	command = ['rsync', '-avz', remote_host, '-e', custom_ssh, '--progress', cwd]
	#command = ['rsync', '-p', str(port), "-e", "\'ssh -i {0}\'".format(key), '$(pwd)', '{0}@{1}:/'.format(user,host)]
	print(' '.join(command))
	#os.system(' '.join(command))

	# proc = subprocess.Popen(command,
	# 					   shell=False,
 #                           stdout=subprocess.PIPE,
 #                           stderr=subprocess.PIPE
 #                           )
	proc = subprocess.check_call(' '.join(command), shell=True)


#JAVA

@main.command()
@click.pass_context
@click.argument('script', type=click.File('r'))
def java(ctx, script):
	'''
	Run java code in the cloud. (UNFINISHED)
	'''
	t = get_cmd()
	click.echo('Beginning spacewire...')
	click.echo('compiler: ' + compiler)
	click.echo('script: ' + script)


#ACCOUNT

@main.group()
def account():
	'''
	Allows you to interact with your SpaceWire account.
	'''
	pass

@account.command()
@pass_config
def whoami(config):
	'''
	Tells you which user you are logged in as.
	'''
	if config.username == '':
		click.echo('Not currently logged in.')
	else:
		click.echo('I am ' + config.username + '.')

@account.command()
def bal():
	'''
	Checks your balance. TODO
	'''
	click.echo('You have 10 credits on this account.')

@account.command()
@pass_config
@click.option('--username', help="Username")
@click.option('--password', help="Password")
def login(config, username, password):
	'''
	Login to account.
	'''
	config.set_username(username)
	config.set_password(password)

@account.command()
@pass_config
def logout(config):
	'''
	Logout from account.
	'''
	config.set_username('')
	config.set_password('')
	click.echo('Logged out.')
	return


@pass_config
def make_rocket(config):
	f = open(config.get_rocketship_path(), 'r')
	file_contents = f.read()
	print (file_contents)
	f.close()

@main.command()
@pass_config
def rocketship(config):
	'''
		Launch a rocketship.
	'''
	make_rocket()
	

