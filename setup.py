from setuptools import setup

setup(

	name='swcli',
	version='0.1',
	py_modules=['run'],
	install_requires=[
		'Click',
		'requests',
		'paramiko'
	],
	entry_points='''
		[console_scripts]
		wire=run:main
	'''
)

