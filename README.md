# SWCLI

Command Line Package for SpaceWire. 


## Setup

Go to the root of the repository, and type in:

```pip install --editable .```

This will setup the swcli as a terminal command.


## Run Python

To run a python script in the cloud simply type


```wire python <script_name>```

So for example if you wanted to run 'build_model.py' on SpaceWire, all you have to do is write

```wire python build_model.py```

## Troubleshooting

```sudo apt-get install -y python-pip python-dev build-essential libssl-dev libffi-dev python3-pip```

