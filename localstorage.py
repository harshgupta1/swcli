
import os
import os.path
import platform
import configparser
#This is where Local Storage will be handled.



class Config(object):

	@staticmethod
	def get_config_path():
		_os = platform.system()
		if _os == 'Darwin':
			return "/Users/samcrane/Library/spacewire"
		elif _os == 'Linux':
			return "/Users/samcrane/Library/spacewire"
		elif _os == 'Windows':
			return "/guy" #TODO I need to figure this one out...

	def __init__(self):
		self.username = ''
		self.password = ''
		self.config_path = Config.get_config_path()
		self.load_config_data()

	def load_config_data(self):
		'''
		Check and see if the user has a config file available.
		'''
		#store it in the library.
		
		self.create_directory()

		if os.path.isfile(self.config_path+'/account.ini'):
			_config = configparser.ConfigParser()
			_config.read(self.config_path+'/account.ini')
			if not 'Account' in _config:
				self.create_config_file()
			else:
				self.username = _config['Account']['username']
				self.password = _config['Account']['password']
		else: 
			print('Account Config file not found.')
			self.create_config_file()
			
	def create_directory(self):
		os.makedirs(self.config_path, exist_ok=True)

	def create_config_file(self):
		with open(self.config_path+'/account.ini', 'w') as file: 
			_config = configparser.ConfigParser()
			_config['Account']={}
			_config['Account']['username'] = ''
			_config['Account']['password'] = ''
			_config.write(file)

	def set_username(self, username):
		_config = configparser.ConfigParser()
		_config.read(self.config_path+'/account.ini')
		with open(self.config_path+'/account.ini', 'w') as file: 
			_config.set('Account', 'username', username)
			_config.write(file)
		return

	def set_password(self, password):
		_config = configparser.ConfigParser()
		_config.read(self.config_path+'/account.ini')
		with open(self.config_path+'/account.ini', 'w') as file: 
			_config.set('Account', 'password', password)
			_config.write(file)
		return

	def get_rocketship_path(self):
		return '/Users/samcrane/Library/spacewire/rocketship.txt'





	#have functionality 



